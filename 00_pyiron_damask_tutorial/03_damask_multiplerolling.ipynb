{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f1e7d1dc-e3d5-423b-b21a-9ffe83a19cb8",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Multiple Rolling Simulation with DAMASK </font>  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "342a274f-b949-4cfc-8dd2-008f930c8cb8",
   "metadata": {},
   "source": [
    "<div class=\"admonition note\" name=\"html-admonition\" style=\"background:#e3f2fd; padding: 10px\">\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> <b> StahlDigital Tutorial: Creating and Running Simulations for Steel Development </b> </font> </br>\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> 25 April 2024 </font>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cc1f7f7-f5c8-48e3-b7b4-950ba2cd2a8e",
   "metadata": {},
   "source": [
    "In this notebook, we will use `pyiron` to setup and run a workflow for multiple rolling simulation of steel with the continuum code [DAMASK](https://damask.mpie.de/release/). A damask simulation requires material specific information (`Elastic` and `Plastic` parameters of the material). We will show, how we can get these parameters from a `Tensile Test Experiment` data using [DSMS](https://stahldigital.materials-data.space/) and run damask simulation with these parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e79d0bd7-bc28-4aba-a8c2-18d5c0b6033d",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Import necessary libraries </font>  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db94105a-253a-4d6e-827f-1fde282f9e2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pylab as plt\n",
    "from pyiron import Project\n",
    "from damask import Rotation\n",
    "from dsms import DSMS, KItem\n",
    "from getpass import getpass\n",
    "from urllib.parse import urljoin\n",
    "from data2rdf import AnnotationPipeline, Parser "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb9ec662-3663-4d5a-800a-c67463350838",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Unit conversion factors: from megapascal(MPa) and gegapascal(GPa) to pascal(Pa)  \n",
    "MPa_to_Pa = 1e+6\n",
    "GPa_to_Pa = 1e+9"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f47d4fe-d51d-48ec-9ea7-228cdd104122",
   "metadata": {},
   "source": [
    "#### <font style=\"font-family:roboto;color:#455e6c\"> Create a pyiron project </font>  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54951d3f-7b2a-48a9-b9c6-466f84362247",
   "metadata": {},
   "outputs": [],
   "source": [
    "pr = Project('damask_rolling_simulation')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "864bf1f6-1fad-4c6a-909c-2236bd0d4fcc",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Running a multiple rolling simulation with DAMASK </font>\n",
    "\n",
    "Suppose your colluge performed a nice tensile test experiment and uploaded the data, the fitted elasticity parameters, and phenopowerlaw parameters required for damask simulation into the `DSMS`. Now, we will show how you can get the required parameters from dsms and run your `DAMASK` simulation with it."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1af5a9eb-1430-4288-b38f-b197a6588711",
   "metadata": {},
   "source": [
    "First, we will write two python functions to get the required data from DSMS\n",
    "- A python function to get experimental elastic parameters \n",
    "- A python function to get experimental plastic parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abfff782-2657-431c-8161-253a1f302b57",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A python function to get experimental elastic parameters from dsms\n",
    "def get_elasticity_data_from_dsms(item):\n",
    "    elasticity_data = {\"type\": \"Hooke\",\n",
    "                       \"C_11\": item.custom_properties.ElasticConstantC11.convert_to('Pa'),\n",
    "                       \"C_12\": item.custom_properties.ElasticConstantC12.convert_to('Pa'),\n",
    "                       \"C_44\": item.custom_properties.ElasticConstantC44.convert_to('Pa')\n",
    "                      }\n",
    "    return elasticity_data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c46c8d6-84e9-4bd1-a7c3-481d9fdf3aa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A python function to get experimental plastic parameters from dsms\n",
    "def get_plasticity_data_from_dsms(item):\n",
    "    plasticity_data = {\"type\": \"phenopowerlaw\",\n",
    "                       \"references\": [\"https://doi.org/10.1016/j.actamat.2014.07.071\",\n",
    "                                       \"https://doi.org/10.1007/BF02900224\"],\n",
    "                       \"output\": [\"xi_sl\", \"gamma_sl\"],\n",
    "                       \"N_sl\": item.hdf5.NumberSlipSystems.get(),\n",
    "                       \"dot_gamma_0_sl\": item.hdf5.ReferenceShearRate.get(),\n",
    "                       \"n_sl\": item.hdf5.Inv_ShearRateSensitivity.get(),\n",
    "                       \"a_sl\": item.hdf5.HardeningExponent.get(),\n",
    "                       \"xi_0_sl\": item.hdf5.InitialCriticalStrength.convert_to('Pa'),\n",
    "                       \"xi_inf_sl\": item.hdf5.FinalCriticalStrength.convert_to('Pa'),\n",
    "                       \"h_0_sl_sl\": item.hdf5.InitialHardening.convert_to('Pa'),\n",
    "                       \"h_sl_sl\": [1, 1.4, 1, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                    1.4,1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                    1.4,1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4]\n",
    "                       }\n",
    "    return plasticity_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "999583c8-e629-46c9-8944-ba803c2a5b94",
   "metadata": {},
   "source": [
    "Now, we have to connect to `dsms`. For the next step you need your dsms `username` and `password`. If you don't have a dsms account, please look [here](https://stahldigital.materials-data.space/support) for help."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0c4d5dc-d75a-4dc3-9c7b-0762e81db52e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# After executing this cell, you have to enter your dsms username \n",
    "username = getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2e5d11f-9c54-4522-92c5-319c88b1de2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# After executing this cell, you have to enter your dsms password\n",
    "password = getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4903c8d2-5d8d-4766-863d-fd12aebe24d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we will connect to dsms\n",
    "dsms = DSMS(host_url=\"https://stahldigital.materials-data.space\",\n",
    "            username=username,\n",
    "            password=password)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc63a848-04af-40b8-8ddf-4506bd5999a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Search for our data item using unique KItem UUID\n",
    "# input_kitem_id = 'ff8ee824-cef6-465c-84af-4d332e73ac64' # dsms KItem UUID for our dataset\n",
    "input_kitem_id = '1ba3712b-68f2-4f2f-89d9-e0b3e4823d48' # New KItem\n",
    "item = dsms[input_kitem_id]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad51b16c-ff58-45bd-a3b7-8ef1510d399c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the functions defined above to get experimental elasticity and plasticity data from dsms\n",
    "elasticity_data = get_elasticity_data_from_dsms(item)\n",
    "plasticity_data = get_plasticity_data_from_dsms(item)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c250e1ca-d125-495d-838e-673351b64db5",
   "metadata": {},
   "source": [
    "Now, we will use these data obtained from `dsms` to create a `damask` simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f169a43-2038-46d9-b727-1caff3128c9b",
   "metadata": {},
   "source": [
    "Again, we start by creating a pyiron job `job_rolling`. For multiple rolling, we need to use the pyiron class `ROLLING`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7080422-6e97-48d7-8b76-f97f20eb32c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "job_rolling = pr.create.job.ROLLING('damask_job')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c603694-608e-4297-bd15-c587cec182d1",
   "metadata": {},
   "source": [
    "Now, we use pyiron functinalities to configure our simulation with parameters obtained from `dsms`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7707c9c-5dab-4c33-b82e-20373cdd088c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Elastic paramaters of the material\n",
    "elasticity = pr.continuum.damask.Elasticity(**elasticity_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b91a652-f6ce-47cf-92f9-d27fb64ec3e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plastic parameters of the material\n",
    "plasticity = pr.continuum.damask.Plasticity(**plasticity_data)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0455d697-04eb-4bd7-bb1e-8af90aac2ae9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define phase of the material\n",
    "phase = pr.continuum.damask.Phase(composition ='DX56D',\n",
    "                                  lattice = 'cI',\n",
    "                                  output_list = ['F', 'P'],\n",
    "                                  elasticity = elasticity, \n",
    "                                  plasticity = plasticity\n",
    "                                 )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31322d37-e28a-4111-b81b-c31b79c1b8d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define homogenization\n",
    "homogenization = pr.continuum.damask.Homogenization(method='SX',\n",
    "                                                    parameters={'N_constituents': 1,\n",
    "                                                                \"mechanical\": {\"type\": \"pass\"}})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2040cbbc-be5e-4cd8-8aba-eba59142b468",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the number of grains and grids\n",
    "grains = 60\n",
    "grids = 16 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f1923bb-6ee3-4af6-8d8d-a8c9134617ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define homogenization\n",
    "rotation = pr.continuum.damask.Rotation(Rotation.from_random,\n",
    "                                        grains)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ceb379cf-fed5-4f75-97bb-f4403b30d400",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Materialpoint configuration\n",
    "material = pr.continuum.damask.Material([rotation],\n",
    "                                        ['DX56D'],\n",
    "                                        phase,\n",
    "                                        homogenization)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a7ea72f-7685-4937-b0fb-d52d8f57a5a3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define grid\n",
    "grid = pr.continuum.damask.Grid.via_voronoi_tessellation(spatial_discretization=grids,\n",
    "                                                         num_grains=grains,\n",
    "                                                         box_size=1.6e-5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8f30b8c-ad73-4a3d-9c99-c2700c0bb9c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign the material and grid to the damask job\n",
    "job_rolling.material = material\n",
    "job_rolling.grid = grid"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecdfb5f9-1048-4602-a6f9-74ad7df39f56",
   "metadata": {},
   "source": [
    "#### <font style=\"font-family:roboto;color:#455e6c\"> Now we are ready to start rolling simulation </font>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbbe8875-6a12-4992-aa50-0321781d3f30",
   "metadata": {},
   "source": [
    "Let's do the first rolling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "813b76aa-6367-4bc0-91c7-5acfd855c0e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define parameters for first rolling\n",
    "reduction_height = 0.05\n",
    "reduction_speed = 5.0e-2\n",
    "reduction_outputs = 250\n",
    "regrid_flag = False\n",
    "damask_exe = ''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7550ce1-43e1-42a0-ab3a-5861d325e82b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run first rolling simulation\n",
    "job_rolling.executeRolling(reduction_height,\n",
    "                           reduction_speed,\n",
    "                           reduction_outputs,\n",
    "                           regrid_flag,\n",
    "                           damask_exe\n",
    "                           )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17662409-eeb4-4b09-a8eb-8baf7b1cc0a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Process the result after first rolling simulation\n",
    "job_rolling.postProcess()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b631b17-4826-477b-9f3a-465e7c4cf417",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "afa2051c-72d7-48cb-8c84-8eddec52038b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the result after first rolling simulation\n",
    "job_rolling.plotStressStrainCurve(0.0,0.60,0.0,6.0e+8) # xmin,xmax, ymin,ymax\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "126aeb39-e4dc-45f3-aa4f-2d7055ec783a",
   "metadata": {},
   "source": [
    "Let's upload the result to dsms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7b6df21-7c7a-4e00-b60b-85cb3362d6ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A function to get stress-strain data from a pyiron damask job as dictionary\n",
    "def get_pyiron_damask_output():\n",
    "    stress_strain_data = {}\n",
    "    stress_strain_data['TrueStrain'] = job_rolling.strain_von_Mises.tolist()\n",
    "    stress_strain_data['TrueStress'] = job_rolling.stress_von_Mises.tolist()\n",
    "    return stress_strain_data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "61eb2c2f-1114-4989-a88b-4e4954aeb65e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# After executing this cell, you have to enter your dsms username \n",
    "username = getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35b63dea-0d07-4713-96db-e31c7e886e52",
   "metadata": {},
   "outputs": [],
   "source": [
    "# After executing this cell, you have to enter your dsms password\n",
    "password = getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41003d93-d10b-4f2e-ba51-4252dcf814c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we will connect to dsms\n",
    "dsms = DSMS(host_url=\"https://stahldigital.materials-data.space\",\n",
    "            username=username,\n",
    "            password=password)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73fa5b1a-7037-4599-aae5-a45e42dc22d3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create new Dataset KItem \n",
    "item_damask_output = KItem(name='DASMASK output file by Ujjal test',\n",
    "                           ktype_id=dsms.ktypes.Dataset,\n",
    "                           annotations = [{'iri':'https://w3id.org/steel/ProcessOntology/TrueStrain',\n",
    "                                            'name':'TrueStrain',\n",
    "                                            'namespace':'https://w3id.org/steel/ProcessOntology'},\n",
    "                                            {'iri':'https://w3id.org/steel/ProcessOntology/TrueStress',\n",
    "                                            'name':'TrueStress',\n",
    "                                            'namespace':'https://w3id.org/steel/ProcessOntology'}],\n",
    "                            #attachments = [{\"name\": \"../resources/Poly_60_16x16x16.vti\"}], # in case you have an output vti, you can place it here\n",
    "                            # linked_kitems = [input_kitem_id]\n",
    "                           ) \n",
    "dsms.commit()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "131f1047-cf93-4ae2-b2c8-60b8b3482564",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(item_damask_output.id)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7a83b41-b06c-4ed5-82f6-fb732d0b4a62",
   "metadata": {},
   "outputs": [],
   "source": [
    "stress_strain_data = get_pyiron_damask_output()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "916dcda6-e33d-4386-aff2-8bbc9662933c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# fill the kitem with stress-strain data\n",
    "base_iri = urljoin(str(dsms.config.host_url), str(item_damask_output.id))\n",
    "download_uri = urljoin(str(dsms.config.host_url), f\"api/knowledge/data_api/{item_damask_output.id}\")\n",
    "\n",
    "pipeline = AnnotationPipeline(\n",
    "    raw_data=stress_strain_data,\n",
    "    mapping={\n",
    "        \"TrueStrain\": {\n",
    "            \"iri\": \"https://w3id.org/steel/ProcessOntology/TrueStrain\",\n",
    "            \"key\": \"TrueStrain\",\n",
    "            \"unit\": \"http://qudt.org/vocab/unit/NUM\",\n",
    "            \"value_location\": \"TrueStrain\"\n",
    "        },\n",
    "        \"TrueStress\": {\n",
    "            \"iri\": \"https://w3id.org/steel/ProcessOntology/TrueStress\",\n",
    "            \"key\": \"TrueStress\",\n",
    "            \"unit\": \"Pa\",\n",
    "            \"value_location\": \"TrueStress\"\n",
    "        }\n",
    "        },\n",
    "        parser=Parser.json,\n",
    "        config = {\n",
    "            \"base_iri\": base_iri,\n",
    "            \"data_download_uri\": download_uri,\n",
    "            \"graph_identifier\": base_iri\n",
    "        }\n",
    "    )\n",
    "\n",
    "item_damask_output.custom_properties = pipeline.plain_metadata\n",
    "item_damask_output.hdf5 = pd.DataFrame(pipeline.time_series)\n",
    "dsms.sparql_interface.subgraph.update(pipeline.graph)\n",
    "dsms.commit()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da3ca64c-1ef3-4c17-b79f-9a8c52f4fe6e",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(dsms[item_damask_output.id].hdf5.TrueStress.convert_to('Pa'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0cfb11d-4178-4b27-9a36-97ae0ebbf478",
   "metadata": {},
   "source": [
    "Now, we will do second rolling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4642deb5-8727-4050-b6b1-cd84a9613de8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define parameters for second rolling\n",
    "reduction_height = 0.1\n",
    "reduction_speed = 4.5e-2\n",
    "reduction_outputs = 300\n",
    "regrid_flag = True\n",
    "damask_exe = ''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "122fc104-e12c-4a3b-88f3-67e9f8ef6aec",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run second rolling simulation\n",
    "job_rolling.executeRolling(reduction_height,\n",
    "                           reduction_speed,\n",
    "                           reduction_outputs,\n",
    "                           regrid_flag,\n",
    "                           damask_exe\n",
    "                           )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a180e420-a635-4e09-89db-b017b3217622",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Process the result after second rolling simulation\n",
    "job_rolling.postProcess()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28e08cad-45ae-49ff-9f77-3e9ffa698901",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the result after second rolling simulation\n",
    "job_rolling.plotStressStrainCurve(0.0,0.60,0.0,6.0e8) # xmin,xmax, ymin,ymax\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fac8ad8a-a77b-4209-9736-ec1c944137ea",
   "metadata": {},
   "source": [
    "Third rolling simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0702aaa4-e037-404d-8d34-6bc604d88372",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define parameters for third rolling\n",
    "reduction_height = 0.1\n",
    "reduction_speed = 4.5e-2\n",
    "reduction_outputs = 350\n",
    "regrid_flag = True\n",
    "damask_exe = ''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f919ffdf-0f27-4d74-bba0-d14c20faf0c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run third rolling simulation\n",
    "job_rolling.executeRolling(reduction_height,\n",
    "                           reduction_speed,\n",
    "                           reduction_outputs,\n",
    "                           regrid_flag,\n",
    "                           damask_exe\n",
    "                           )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ef7ed31-bae2-4655-aff3-c0e279b394bf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Process the result after third rolling simulation\n",
    "job_rolling.postProcess()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b091cd24-4dc4-40b3-a7fe-f21c94cfedcb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the result after third rolling simulation\n",
    "job_rolling.plotStressStrainCurve(0.0,0.60,0.0,6.0e+8) # xmin,xmax, ymin,ymax\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d95a445-b83c-4afa-9d35-fcd41d5c0d6a",
   "metadata": {},
   "source": [
    "Forth rolling simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7e25f6e-8290-469c-b1e3-5c8dd9d57c17",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define parameters for forth rolling\n",
    "reduction_height = 0.12\n",
    "reduction_speed = 4.25e-2\n",
    "reduction_outputs = 300\n",
    "regrid_flag = True\n",
    "damask_exe = ''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "44f66a76-2b28-483c-9ba0-528961317dbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run forth rolling simulation\n",
    "job_rolling.executeRolling(reduction_height,\n",
    "                           reduction_speed,\n",
    "                           reduction_outputs,\n",
    "                           regrid_flag,\n",
    "                           damask_exe\n",
    "                           )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea1df326-da41-4099-bd62-6d72509f6abc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Process the result after forth rolling simulation\n",
    "job_rolling.postProcess()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3548cd0f-7b4a-4f2b-884d-498426684c03",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the result after forth rolling simulation\n",
    "job_rolling.plotStressStrainCurve(0.0,0.60,0.0,6.0e+8) # xmin,xmax, ymin,ymax\n",
    "plt.show();"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
