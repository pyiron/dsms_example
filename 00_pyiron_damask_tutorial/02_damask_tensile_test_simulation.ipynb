{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8107054a-2f06-4a51-b724-a06913231430",
   "metadata": {},
   "source": [
    "## <font style=\"font-family:roboto;color:#455e6c\"> Tensile Test Simulation with DAMASK </font>  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79a7684c-0c1a-40d3-be2c-853c12dd08d3",
   "metadata": {},
   "source": [
    "<div class=\"admonition note\" name=\"html-admonition\" style=\"background:#e3f2fd; padding: 10px\">\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> <b> StahlDigital Tutorial: Creating and Running Simulations for Steel Development </b> </font> </br>\n",
    "<font style=\"font-family:roboto;color:#455e6c\"> 25 April 2024 </font>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31110557-6779-45d5-87a8-b34ca72bcc04",
   "metadata": {},
   "source": [
    "In this notebook, we will use `pyiron` to setup and run a workflow to calculate the stress-strain curve of a material with the continuum code [DAMASK](https://damask.mpie.de/release/). A damask simulation requires material specific information (`Elastic` and `Plastic` parameters of the material). We will also show, how we can calculate the elastic parameters within pyiron with `Lammps` and use them to run `Damask` simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "357266ff-e392-41d9-9015-b4979755f8ca",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Import necessary libraries </font>  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25102d51-efce-4bc6-adbd-cc5a3061f8d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pylab as plt\n",
    "from pyiron import Project\n",
    "from damask import Rotation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c43d2fb-d3ef-4b64-9410-3286753df259",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Unit conversion factors: from megapascal(MPa) and gegapascal(GPa) to pascal(Pa)  \n",
    "MPa_to_Pa = 1e+6\n",
    "GPa_to_Pa = 1e+9"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb608e83-9007-4e1a-a3eb-09bd2d7ae73f",
   "metadata": {},
   "source": [
    "#### <font style=\"font-family:roboto;color:#455e6c\"> Create a pyiron project </font>  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f484be4-445c-4708-a72f-7b6691e518bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "pr = Project(\"damask_tensile_simulation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b776af78-2bfd-431f-8948-cf28e302f164",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Running a continuum tensile test calculation (with DAMASK) </font>\n",
    "\n",
    "We can set up a simulation \"job\" of any continuum simulation that is intergrated within pyiron. In this section, we are going to use the continuum code [DAMASK](https://damask.mpie.de/release/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15853165-fc06-4324-b7c2-a155482efc7d",
   "metadata": {},
   "source": [
    "Let's start by creating a damask job. For tensile test simulation, we need to use the pyiron class `DAMASK`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d54c662-3745-49a9-a2f5-f63f5f8030f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a damask job\n",
    "job_damask = pr.create.job.DAMASK(\"damask_simulation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9e374dd-d8a5-4326-90c1-8592723eb4c9",
   "metadata": {},
   "source": [
    "A DAMASK simulation requires two sets of configuration files: \n",
    "1. Generic files for all solvers in `YAML` style. A mandatory materialpoint configuration file (`material.yaml`) and a optional numerics configuration file (`numerics.yaml`)\n",
    "2. Solver-specific files for geometry and load case.\n",
    "\n",
    "More information are available [here](https://damask.mpie.de/release/documentation/index.html)\n",
    "\n",
    "Once, we pass the required parameter values to `pyiron` and generation of damask specific input files is handeled by `pyiron` internally when we call the `run()` method on our job. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7707c9c-5dab-4c33-b82e-20373cdd088c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Elastic paramaters of the material\n",
    "elasticity = pr.continuum.damask.Elasticity(type = 'Hooke',\n",
    "                                            C_11 = 226000000000.0,\n",
    "                                            C_12 = 140000000000.0,\n",
    "                                            C_44 = 116000000000.0\n",
    "                                           )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b91a652-f6ce-47cf-92f9-d27fb64ec3e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plastic parameters of the material\n",
    "plasticity = pr.continuum.damask.Plasticity(type = 'phenopowerlaw',\n",
    "                                            references = [\"https://doi.org/10.1016/j.actamat.2014.07.071\",\n",
    "                                                          \"https://doi.org/10.1007/BF02900224\"],\n",
    "                                            output = [\"xi_sl\", \"gamma_sl\"],\n",
    "                                            N_sl = [12, 12],\n",
    "                                            dot_gamma_0_sl = [0.001, 0.001],\n",
    "                                            n_sl = [20, 20],\n",
    "                                            a_sl = [3.3838750211834414, 2.71513775941709],\n",
    "                                            xi_0_sl = [58854884.55400854, 116398683.46869445],\n",
    "                                            xi_inf_sl = [245397260.8849832, 289183253.8512693],\n",
    "                                            h_0_sl_sl = [839702224.3104236, 797832430.2007155],\n",
    "                                            h_sl_sl = [1, 1.4, 1, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                                       1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                                       1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4]\n",
    "                                            )  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0455d697-04eb-4bd7-bb1e-8af90aac2ae9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define phase of the material\n",
    "phase = pr.continuum.damask.Phase(composition ='DX56D',\n",
    "                                  lattice = 'cI',\n",
    "                                  output_list = ['F', 'P'],\n",
    "                                  elasticity = elasticity, \n",
    "                                  plasticity = plasticity\n",
    "                                 )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31322d37-e28a-4111-b81b-c31b79c1b8d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define homogenization\n",
    "homogenization = pr.continuum.damask.Homogenization(method='SX',\n",
    "                                                    parameters={'N_constituents': 1,\n",
    "                                                                \"mechanical\": {\"type\": \"pass\"}})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2040cbbc-be5e-4cd8-8aba-eba59142b468",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the number of grains and grids\n",
    "grains = 60\n",
    "grids = 16 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f1923bb-6ee3-4af6-8d8d-a8c9134617ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define homogenization\n",
    "rotation = pr.continuum.damask.Rotation(Rotation.from_random,\n",
    "                                        grains)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ceb379cf-fed5-4f75-97bb-f4403b30d400",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Materialpoint configuration\n",
    "material = pr.continuum.damask.Material([rotation],\n",
    "                                        ['DX56D'],\n",
    "                                        phase,\n",
    "                                        homogenization)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a7ea72f-7685-4937-b0fb-d52d8f57a5a3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define grid\n",
    "grid = pr.continuum.damask.Grid.via_voronoi_tessellation(spatial_discretization = grids,\n",
    "                                                         num_grains = grains,\n",
    "                                                         box_size = 1.6e-5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8f30b8c-ad73-4a3d-9c99-c2700c0bb9c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign the material and grid to the damask job\n",
    "job_damask.material = material\n",
    "job_damask.grid = grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "666b027a-9993-4676-be3e-5b15e1621bdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# List solvers available fo this job\n",
    "job_damask.list_solvers()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17f60e4d-4bec-42ba-8429-8190bbc44230",
   "metadata": {},
   "outputs": [],
   "source": [
    "# We choose the first solver\n",
    "solver = job_damask.list_solvers()[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7379b818-4197-4623-93d5-4f545a390d3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate a load case for the grid solver\n",
    "load_step =[{'mech_bc_dict' : \n",
    "             {'dot_F' : [1e-3, 0, 0,\n",
    "                         0, 'x', 0,\n",
    "                         0, 0, 'x'],\n",
    "              'P' : ['x', 'x', 'x',\n",
    "                     'x', 0, 'x',\n",
    "                     'x', 'x', 0]\n",
    "              },\n",
    "             'discretization' : {'t' : 10., 'N' : 40},\n",
    "             'additional': {'f_out' : 4}\n",
    "             },\n",
    "            {'mech_bc_dict' : \n",
    "             {'dot_F' : [1e-3, 0, 0,\n",
    "                         0, 'x', 0,\n",
    "                         0, 0, 'x'],\n",
    "              'P':['x', 'x', 'x',\n",
    "                   'x', 0, 'x',\n",
    "                   'x', 'x', 0]\n",
    "             },\n",
    "             'discretization' : {'t' : 50., 'N' : 50}, # Use t=250. and N=250\n",
    "             'additional': {'f_out' : 4}\n",
    "             }]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3fc39251-aec0-448a-b6da-1909f6160df4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign solver and load step to the damask job\n",
    "job_damask.loading = pr.continuum.damask.Loading(solver=solver,\n",
    "                                                 load_steps=load_step\n",
    "                                                 )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85176a1b-0245-420e-bc3c-8df4b7683d66",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# Run the simulation\n",
    "job_damask.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d691c5d-10df-465d-8270-294f07f0da64",
   "metadata": {},
   "outputs": [],
   "source": [
    "# get a quick overview of jobs within this project\n",
    "pr.job_table()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "65ae9a03-d4cf-4eb8-88f0-154105ca4ad9",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e7b0ce0-18e8-4b3c-a5bc-d3c37b9200be",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot stress vs strain curve\n",
    "job_damask.plot_stress_strain(von_mises=True)\n",
    "plt.xlabel(\"strain_von_Mises\")\n",
    "plt.ylabel(\"stress_von_Mises (Pa)\")\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "142da152-66d0-4027-b9ee-0259b07b7c18",
   "metadata": {},
   "source": [
    "Let's compare our damask simulated stress-strain curve with the experimental curve"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "265a5fcd-b911-4c99-9244-f858623392aa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the experimental stress-strain data\n",
    "exp_data = np.loadtxt(\"../resources/true_stress_strain.txt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "930be774-f528-4bec-b038-b00045ddbe2e",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Plot damask calculated stress-strain curve along with the experimental curve\n",
    "plt.plot(job_damask.output.strain[:, 0, 0], job_damask.output.stress[:, 0, 0],\n",
    "         '--o', label=\"DAMASK\")\n",
    "plt.plot(exp_data[:,1][:12000], exp_data[:,0][:12000]*MPa_to_Pa, label=\"Experimental\")\n",
    "plt.xlabel(\"Strain\")\n",
    "plt.ylabel(\"Stress (Pa)\")\n",
    "plt.legend()\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "raw",
   "id": "02618ca3-9d2e-4de5-b1a2-8f6ac3f2001d",
   "metadata": {},
   "source": [
    "job_damask.output"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9f5dfc6-445e-46c0-87c1-46b3e22361f6",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Run DAMASK simulation with LAMMPS calculated elastic parameters </font>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b646711f-34d2-4c49-92c1-e78c7ed27d6c",
   "metadata": {},
   "source": [
    "Let's create two python functions to make our life easier\n",
    "- A first python function which takes any bulk structure and corresponding potential file name as input and calculate the elastic tensors with `LAMMPS`\n",
    "- A second python function which takes the elastic tensors from the first python function as input and perform a tensile test simulation with `DAMASK`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38980540-33f9-469b-8c1b-4191a72ca652",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A python function to run LAMMPS\n",
    "def calc_elastic_tensors_lammps(structure, \n",
    "                                potential=\"2005--Mendelev-M-I--Al-Fe--LAMMPS--ipr1\",\n",
    "                                delete_existing_job=False\n",
    "                               ):\n",
    "    \"\"\"\n",
    "    This function takes atomic structure as input argument and returns\n",
    "    elastic tensor values for the structure.\n",
    "    \"\"\"\n",
    "    # Create and optimizate the structure\n",
    "    job_mini = pr.create.job.Lammps(job_name='lammps_mini_job',\n",
    "                                    delete_existing_job=delete_existing_job)\n",
    "    job_mini.structure = structure\n",
    "    job_mini.potential = potential\n",
    "    job_mini.calc_static()\n",
    "    job_mini.run()\n",
    "    \n",
    "    # Create a reference job \n",
    "    job_ref = pr.create.job.Lammps(job_name='lammps_ref_job',\n",
    "                                   delete_existing_job=delete_existing_job)\n",
    "    job_ref.structure = job_mini.get_structure(-1) # Assign the already optimized structure to this job\n",
    "    job_ref.potential = potential\n",
    "    job_ref.calc_static()\n",
    "    \n",
    "    # Create a `ElasticTensor` job and assign `job_ref` to this job\n",
    "    job_elastic = pr.create.job.ElasticTensor(job_name=\"elastic_tensor_job\",\n",
    "                                              delete_existing_job=delete_existing_job)\n",
    "    job_elastic.ref_job = job_ref\n",
    "    \n",
    "    # Call the run function\n",
    "    job_elastic.run()\n",
    "    \n",
    "    output = {\"type\": \"Hooke\",\n",
    "              \"C_11\": job_elastic['output/elastic_tensor'][0][0]*GPa_to_Pa,\n",
    "              \"C_12\": job_elastic['output/elastic_tensor'][0][1]*GPa_to_Pa,\n",
    "              \"C_44\": job_elastic['output/elastic_tensor'][3][3]*GPa_to_Pa}\n",
    "    \n",
    "    return output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07b81ac4-a99c-495c-b8b4-75a57c044c9b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A python function to run DAMASK\n",
    "def run_damask_simulation(elasticity_data, \n",
    "                          plasticity_data,\n",
    "                          delete_existing_job=False):\n",
    "    \"\"\"\n",
    "    This function takes elastic tensors as input argument and returns\n",
    "    strain and stress values of damask tensile simulation.\n",
    "    \"\"\"\n",
    "    # Create a damask job\n",
    "    job_damask = pr.create.job.DAMASK(\"damask_simulation_lammps\", \n",
    "                                      delete_existing_job=delete_existing_job)\n",
    "    \n",
    "    # Elastic paramaters of the material\n",
    "    elasticity = pr.continuum.damask.Elasticity(**elasticity_data)\n",
    "    \n",
    "    # Plastic parameters of the material\n",
    "    plasticity = pr.continuum.damask.Plasticity(**plasticity_data)\n",
    "    \n",
    "    # Define phase of the material\n",
    "    phase = pr.continuum.damask.Phase(composition ='DX56D',\n",
    "                                      lattice = 'cI',\n",
    "                                      output_list = ['F', 'P'],\n",
    "                                      elasticity = elasticity, \n",
    "                                      plasticity = plasticity\n",
    "                                     )\n",
    "    # Define homogenization\n",
    "    homogenization = pr.continuum.damask.Homogenization(method='SX',\n",
    "                                                        parameters={'N_constituents': 1,\n",
    "                                                                    \"mechanical\": {\"type\": \"pass\"}})\n",
    "    # Defines the number of grains and grids\n",
    "    grains = 60\n",
    "    grids = 16 \n",
    "    \n",
    "    # Define homogenization\n",
    "    rotation = pr.continuum.damask.Rotation(Rotation.from_random,\n",
    "                                            grains)\n",
    "    # Materialpoint configuration\n",
    "    material = pr.continuum.damask.Material([rotation],\n",
    "                                            ['DX56D'],\n",
    "                                            phase,\n",
    "                                            homogenization)\n",
    "    # Define grid\n",
    "    grid = pr.continuum.damask.Grid.via_voronoi_tessellation(spatial_discretization=grids,\n",
    "                                                             num_grains=grains,\n",
    "                                                             box_size=1.6e-5)\n",
    "    \n",
    "    # Assign the material and grid to the damask job\n",
    "    job_damask.material = material\n",
    "    job_damask.grid = grid\n",
    "    \n",
    "    # Generate a load case for the grid solver\n",
    "    load_step =[{'mech_bc_dict' : \n",
    "                 {'dot_F' : [1e-3, 0, 0,\n",
    "                             0, 'x', 0,\n",
    "                             0, 0, 'x'],\n",
    "                  'P' : ['x', 'x', 'x',\n",
    "                         'x', 0, 'x',\n",
    "                         'x', 'x', 0]\n",
    "                  },\n",
    "                 'discretization' : {'t' : 10., 'N' : 40},\n",
    "                 'additional': {'f_out' : 4}\n",
    "                 },\n",
    "                {'mech_bc_dict' : \n",
    "                 {'dot_F' : [1e-3, 0, 0,\n",
    "                             0, 'x', 0,\n",
    "                             0, 0, 'x'],\n",
    "                  'P':['x', 'x', 'x',\n",
    "                       'x', 0, 'x',\n",
    "                       'x', 'x', 0]\n",
    "                 },\n",
    "                 'discretization' : {'t' : 50., 'N' : 50}, # Use t=250. and N=250\n",
    "                 'additional': {'f_out' : 4}\n",
    "                 }]\n",
    "    \n",
    "    # Assign solver and load step to the damask job\n",
    "    job_damask.loading = pr.continuum.damask.Loading(solver=job_damask.list_solvers()[0],\n",
    "                                                     load_steps=load_step\n",
    "                                                     )\n",
    "    \n",
    "    # Run the simulation\n",
    "    job_damask.run()\n",
    "\n",
    "    return job_damask.output.strain[:, 0, 0], job_damask.output.stress[:, 0, 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72592753-a361-4251-bcd6-8ef4c52efcdb",
   "metadata": {},
   "source": [
    "#### <font style=\"font-family:roboto;color:#455e6c\"> We will use the python functions defined above to perform our simulation </font>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36a0f1ca-2c4a-44f4-b1b5-38beed706639",
   "metadata": {},
   "source": [
    "First, create a cubic unit cell of bcc iron and use `calc_elastic_tensors_lammps()` to calculate the elastic tensors "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b13d48c-8bed-4a8e-bd1f-5181ac609e65",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Complete the code below to create a atomic structure of bcc iron\n",
    "my_structure = "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e68b593f-b075-4867-a318-f518c3a32586",
   "metadata": {},
   "source": [
    "Pass `my_structure` to `calc_elastic_tensors_lammps()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d59b512-b41d-40fc-9d32-e80d25ded61d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate elastic tensors\n",
    "elasticity_data = calc_elastic_tensors_lammps(structure=my_structure)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3cbfafd8-d28b-4876-81bd-f5dee497e4a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set plasticity parameters of the material\n",
    "plasticity_data = {\"type\": \"phenopowerlaw\",\n",
    "                   \"references\": [\"https://doi.org/10.1016/j.actamat.2014.07.071\",\n",
    "                                   \"https://doi.org/10.1007/BF02900224\"],\n",
    "                   \"output\": [\"xi_sl\", \"gamma_sl\"],\n",
    "                   \"N_sl\": [12, 12],\n",
    "                   \"dot_gamma_0_sl\": [0.001, 0.001],\n",
    "                   \"n_sl\": [20, 20],\n",
    "                   \"a_sl\": [3.3838750211834414, 2.71513775941709],\n",
    "                   \"xi_0_sl\": [58854884.55400854, 116398683.46869445],\n",
    "                   \"xi_inf_sl\": [245397260.8849832, 289183253.8512693],\n",
    "                   \"h_0_sl_sl\": [839702224.3104236, 797832430.2007155],\n",
    "                   \"h_sl_sl\": [1, 1.4, 1, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                1.4,1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4,\n",
    "                                1.4,1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4]\n",
    "                   }"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41526b9e-8b63-4b27-81cc-b111989137f4",
   "metadata": {},
   "source": [
    "Pass the `elasticity_data` and `elasticity_data` to `run_damask_simulation()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d53f26a-84a7-49aa-ac19-76bb4c706462",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Execute this cell to get damask calculated stress and strain\n",
    "result = run_damask_simulation(elasticity_data, plasticity_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "062e9df8-d940-46fe-a4af-e71f637a4517",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot damask calculated stress-strain curve along with the experimental curve\n",
    "plt.plot(result[0], result[1], '--o', label=\"DAMASK_LAMMPS\")\n",
    "plt.plot(exp_data[:,1][:12000], exp_data[:,0][:12000]*MPa_to_Pa, label=\"Experimental\")\n",
    "plt.xlabel(\"Strain\")\n",
    "plt.ylabel(\"Stress (Pa)\")\n",
    "plt.legend()\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b7eb74b-6731-4e19-90d3-c78af3d916ee",
   "metadata": {},
   "source": [
    "### <font style=\"font-family:roboto;color:#455e6c\"> Software used in this notebook </font>\n",
    "\n",
    "- [pyiron_atomistics](https://github.com/pyiron/pyiron_atomistics)\n",
    "- [pyiron_continuum](https://github.com/pyiron/pyiron_continuum)\n",
    "- [LAMMPS](https://www.lammps.org/)\n",
    "- [DAMASK](https://damask.mpie.de/release)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
